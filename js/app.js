/**
 * custom.js
 * custom JavaScript functions and plugin initialization
 */
jQuery(function ($) {

    "use strict";

    // Add accordion like submenu items to responsive menu
    function responsive_menu_items() {
        var sub_menus = $('.resp-menu').find('.menu-item-has-children, .page_item_has_children'),
            expand_menus;

        if (sub_menus.length) {
            $(sub_menus).each(function () {
                $(this).append('<a class="expand-menu" href="#"><i class="mdi mdi-keyboard_arrow_down"></i></a>').find('.sub-menu').hide();
            });
        }

        expand_menus = $('.resp-menu').find('.expand-menu');

        if (expand_menus.length) {
            $(expand_menus).on('click', function (e) {
                var icon = $(this).find('.mdi');
                icon.toggleClass('rotate-180');
                $(this).prev().slideToggle(300);
                e.preventDefault();
            });
        }

    }

    responsive_menu_items();


    // Sticky Nav
    function sticky_nav() {
        var lastScroll = 0,
            top_bar = $('#utility-top').outerHeight() + $('.top-widget-area').outerHeight();


        $(window).on('load scroll', function () {
            var st = $(this).scrollTop();
            if (st > top_bar) {
                $('#header').addClass('nav-enabled');
                $('#scroll-to-top').addClass("scrolled");
            } else {
                $('#header').removeClass('nav-enabled');
                $('#scroll-to-top').removeClass("scrolled");
            }
            lastScroll = st;
        });
    }

    sticky_nav();


});

jQuery(document).ready(function ($) {

    "use strict";

    /* =========================================================
     01. Blocking Layer + Spinner
     ============================================================ */
    function openLoadingOverlay() {
        $("#blockingOverlay").show();
    }

    function closeLoadingOverlay() {
        $("#blockingOverlay").addClass('fadeOut').one('animationend webkitAnimationEnd', function () {
            $(this).hide().removeClass('fadeOut');
        });
    }


    /**
     * Animate items as they appear
     * uses jQuery viewport checker plugin
     */

    function animateWhenVisible() {
        var anim_items = $('.animated'),
            no_animation = $('body').hasClass('no-animation');
        if (anim_items.length && !no_animation && $.fn.viewportChecker) {
            anim_items.addClass('invisible').viewportChecker({
                classToAdd: null,
                classToRemove: null,
                offset: 100,
                callbackFunction: function (elem) {
                    var animclass = elem.data('animation'),
                        animdelay = elem.data('animdelay');
                    if (animdelay) {
                        setTimeout(function () {
                            elem.addClass('visible ' + animclass);
                        }, animdelay);
                    } else {
                        elem.addClass('visible ' + animclass);
                    }
                }
            });
        }
    }

    animateWhenVisible();

    // Image Background using data-bgimage="xx"
    function sectionbg() {
        var elem = $('.section');
        elem.each(function () {
            var t = $(this),
                bgimage = $(t).data('bgimage');
            if (bgimage) {
                $(t).css({
                    backgroundImage: 'url(' + bgimage + ')'
                });
            }
        });
    }

    sectionbg();

    // Overlay background and color using data-overlay-bg="xx" and data-overay-color="xx"
    function overlaycolors() {
        var elem = $('.color-overlay');
        elem.each(function () {
            var t = $(this),
                overlaybg = $(t).data('overlaybg'),
                overlaycolor = $(t).data('overlaycolor');
            if (overlaybg || overlaycolor) {
                $(t).css({
                    backgroundColor: overlaybg,
                    color: overlaycolor
                });
            }
        });
    }

    overlaycolors();

    //toggle Flip Container
    $('.flip-container .switch-field label').on('click', function (e) {
        e.preventDefault();
        console.log("checked");
        $(this).closest('.flip-container').toggleClass('hover');
    });

    //responsive menu trigger
    $('.menu-trigger, .resp-menu-mask').click(function (e) {
        $('body').toggleClass('show-nav');
        e.preventDefault();
    });

    //close

    // Search form
    $('.search-trigger').on('click', function (e) {
        e.preventDefault();
        openLoadingOverlay();
        $(this).toggleClass('search-btn-active');
        $('.search-panel').toggleClass('search-active');
        setTimeout(function () {
            $('.search-panel').find('input.search-field').focus();
        }, 500);
    });

    $('.search-close').on('click', function (e) {
        e.preventDefault();
        closeLoadingOverlay();
        $('.search-panel').removeClass('search-active');
        $('.search-trigger').removeClass('search-btn-active');
    });

    $('a[href*="#"]:not([href="#"])').click(function () {
        if ($(this).hasClass("nav-link")) {
            return false;
        }
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: target.offset().top
                }, 600);
                return false;
            }
        }
    });


    // WooCommerce cart submenu
    $(document).on('click', 'a.cart-contents-2', function (e) {
        $(this).parent().find('div.cart-submenu').toggleClass('cart-active');
        $(this).toggleClass('cart-btn-active');
        e.preventDefault();
    });

    // fix broken ProductTab
    $('.woocommerce-tabs a').on('click', function (e) {
        e.preventDefault();
        $('ul.tabs li').removeClass("active");
        $(this).parent().addClass("active");
        var target = $(this.hash);
        $('.woocommerce-Tabs-panel').hide();
        $(target).show();
    });

    // bikeOverview tabs
    $('a.nav-link').on('click', function (e) {
        e.preventDefault();
        $('a.nav-link').removeClass("active");
        $(this).addClass("active");
        var target = $(this.hash);
        $('.tab-pane').removeClass("active");
        $(target).addClass("active");
        var imageUrl = $('.section.parallax').data('bgimage');
        var baseUrl = imageUrl.substr(0, imageUrl.lastIndexOf("/"));
        var newUrl = baseUrl + "/" + $(this).text().toLowerCase().replace(/\s/g, '') + '.jpg';
        $('.section').css("background-image", 'url(' + newUrl + ')');
        $('.section').find('h2').text($(this).text());
    });

    // bad hack to show sofort lieferbar in green.
    $(".single_variation_wrap").on("show_variation", function (event, variation) {
        if (variation.delivery_time.indexOf("sofort lieferbar") >= 0) {
            setTimeout(function () {
                $('.entry-summary .wc-gzd-additional-info.delivery-time-info').html('Lieferzeit: <span class="in_stock">Sofort lieferbar</span> - Wenige Exemplare auf Lager | Lieferzeit ca. 2-4 Werktage!');
            }, 0);
        }
    });


    var $masonry = $('.blog-style-masonry').isotope({
        itemSelector: '.post-item',
        columnWidth: 0,
        gutter: 0,
    }).isotope('reloadItems');
    $(window).load(function () {
        setTimeout(function () {
            $masonry.isotope('layout');
        }, 0);
    });

    var target = $(".product-carousel");
    if (target.length) {
        $(target).each(function () {
            var slider = $(this).find(".products"),
                parent = $(this);
            $(slider).owlCarousel({
                items: $(parent).data('items'),
                loop: $(parent).data('loop'),
                margin: $(parent).data('margin'),
                autoplay: $(parent).data('autoplay'),
                autoplayTimeout: $(parent).data('timeout'),
                autoHeight: $(parent).data('autoheight'),
                nav: $(parent).data('nav'),
                dots: $(parent).data('dots'),
                smartSpeed: $(parent).data('speed'),
                navText: false,
                autoplayHoverPause: true,
                animateIn: $(parent).data('animatein'),
                animateOut: $(parent).data('animateout'),

                responsive: {
                    0: {
                        items: 1,
                        margin: ($(parent).data('items') == 1 ? 0 : $(parent).data('margin_mobile')),
                        dots: false
                    },
                    480: {
                        items: ($(parent).data('items') > 2 ? 2 : $(parent).data('items')),
                        margin: $(parent).data('margin_mobile'),
                        dots: false
                    },
                    720: {
                        items: ($(parent).data('items') > 3 ? 3 : $(parent).data('items')),
                        margin: $(parent).data('margin_mobile'),
                        dots: $(parent).data('dots') ? true : false
                    },
                    960: {
                        items: $(parent).data('items')
                    }
                }
            });
        });
    }


    $('.dropdown-toggle').dropdown();

    $('.dropdown-menu').click(function (e) {
        e.stopPropagation();
    });

    if ($('.homepage-slider').length > 0) {
        var items = $(".homepage-slider .navigation .dot");
        var timeout = $(".homepage-slider").data("timeout");
        changeImage(items[Math.floor(Math.random() * items.length)]);
    }

    function changeImage(dot) {
        dot.click();
        setTimeout(function () {
            changeImage(items[Math.floor(Math.random() * items.length)]);
        }, timeout);
    }

});

