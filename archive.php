<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Serpentine 1.0
 */

get_header(); ?>


<main id="main" class="col-xs-12 col-md-9" role="main"><div class="blog-style-masonry">
	<?php
	// Start the loop.
	while ( have_posts() ) : the_post();

		// Include the single post content template.
		get_template_part( 'template-parts/content', '' );

		/*// If comments are open or we have at least one comment, load up the comment template.
		if ( comments_open() || get_comments_number() ) {
			comments_template();
		}*/

		if ( is_singular( 'post' ) ) {
			// Previous/next post navigation.
			the_post_navigation( array(
				'next_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Next', 'serpentine' ) . '</span> ' .
					'<span class="screen-reader-text">' . __( 'Next post:', 'serpentine' ) . '</span> ' .
					'<span class="post-title">%title</span>',
				'prev_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Previous', 'serpentine' ) . '</span> ' .
					'<span class="screen-reader-text">' . __( 'Previous post:', 'serpentine' ) . '</span> ' .
					'<span class="post-title">%title</span>',
			) );
		}

		// End of the loop.
	endwhile;
    echo  '</div>';
    // Previous/next page navigation.
    the_posts_pagination(array(
        'prev_text' => __('Previous page', 'serpentine'),
        'next_text' => __('Next page', 'serpentine'),
        'before_page_number' => '<span class="meta-nav screen-reader-text">' . __('Page', 'serpentine') . ' </span>',
    ));
	?>

</main><!-- .site-main -->

<?php get_sidebar(); ?>

<?php get_footer(); ?>

