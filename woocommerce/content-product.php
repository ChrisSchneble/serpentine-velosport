<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

if (!defined('ABSPATH')) exit; // Exit if accessed directly

global $product, $post;

// Ensure visibility
if (empty($product) || !$product->is_visible()) {
    return;
}

$classes[] = 'col-xs-6 col-sm-4';
?>
<div <?php post_class($classes); ?>>
    <div class="ss-product-collection">
        <div class="image-set"><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('shopItem-thumb'); ?></a></div>
        <div class="collection-category"><?php echo get_cat_name(getFirstCatId($post)); ?></div>
        <div class="collection-description"><h2 title="<?php the_title() ?>"><?php the_title_limit(20, '...'); ?></h2>
            <p class="collection-excerpt"><?php echo excerpt(10); ?></p>
            <div class="collection-actions">
                <a class="collection-action-btn" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">Details
                    ansehen</a><?php echo productNewFlag($product) ?><?php echo productOnSaleFlag($product) ?><span class="price-range"><?php echo printActualPrice($product); ?></span></div>
        </div>
    </div>
</div>

