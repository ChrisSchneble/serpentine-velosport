<?php
/**
 * WooCommerce hooks
 * Custom functions, hooks and actions for WooCommerce Plugin
 */


add_action( 'wp', 'serpentine_remove_sidebar_product_pages' );

function serpentine_remove_sidebar_product_pages() {
    if (is_product()) {
    remove_action('woocommerce_sidebar','woocommerce_get_sidebar',10);
    }
}

remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );


// Update cart fragments in top nav
function top_nav_fragments($fragments)
{
    ob_start(); ?>
    <a class="cart-contents" href="<?php echo wc_get_cart_url(); ?>"
       title="<?php esc_attr_e('View your shopping cart', 'serpentine'); ?>">
        <i class="mdi mdi-shopping_cart"></i><?php printf(_n('%d item', '%d items', WC()->cart->get_cart_contents_count()), WC()->cart->get_cart_contents_count(), 'bonanza');
        echo WC()->cart->get_cart_total(); ?></a>
    <?php
    $fragments['a.cart-contents'] = ob_get_clean();
    return $fragments;
}

add_filter('woocommerce_add_to_cart_fragments', 'top_nav_fragments');

// Update cart fragments in shopping bag
function shopping_bag_fragments($fragments)
{
    ob_start(); ?>
    <a class="cart-contents-2" href="#" title="<?php esc_attr_e('View your shopping cart', 'serpentine'); ?>"><i
                class="mdi mdi-shopping_cart"></i><span
                class="cart-count"><?php echo esc_attr(WC()->cart->get_cart_contents_count()); ?></span></a>
    <?php
    $fragments['.cart-contents-2'] = ob_get_clean();
    return $fragments;
}

add_filter('woocommerce_add_to_cart_fragments', 'shopping_bag_fragments');

//change Tab name
function rename_products_additional_information_tab($tabs) {
	// Das Tab "Weitere informationen" in "Spezifikationen" umbenennen
	$tabs['additional_information']['title'] = 'Spezifikationen';

	return $tabs;
}
add_filter('woocommerce_product_tabs', 'rename_products_additional_information_tab');

// Content wrappers
function shop_wrapper_start()
{
    if (is_singular('product')) {
        echo '<main id="main" class="col-xs-12 clearfix">';
    } else {
        echo '<main id="main" class="col-xs-12 col-md-9 clearfix">';
    }
}

function shop_wrapper_end()
{
    echo '</main>';
}

remove_action('woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action('woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);
add_action('woocommerce_before_main_content', 'shop_wrapper_start', 10);
add_action('woocommerce_after_main_content', 'shop_wrapper_end', 10);


/** change number of related products */
add_filter( 'woocommerce_output_related_products_args', 'serpentine_related_products_args', 20 );
function serpentine_related_products_args( $args ) {
$args['posts_per_page'] = 3; // 4 related products
$args['columns'] = 1; // arranged in 4 columns
return $args;
}


/**
 * Modify WooCommerce product loop wrapper
 * ul is changed to div
 * inner items are div instead of li
 */

function woocommerce_product_loop_start() {
	printf ( '<div class="products column-3">');
}

function woocommerce_product_loop_end() {
	printf ( '</div><!-- /.products -->' );
}


// Disable automatically generated page titles by WooCommerce
add_filter('woocommerce_show_page_title', function () {
    return false;
});

// Change breadcrumb separator

function change_breadcrumb_delim($defaults)
{
    $defaults['delimiter'] = '<span class="delimiter"></span>';
    return $defaults;
}

add_filter('woocommerce_breadcrumb_defaults', 'change_breadcrumb_delim');

add_filter( 'woocommerce_variable_sale_price_html', 'serpentine_variation_price_format', 10, 2 );

add_filter( 'woocommerce_variable_price_html', 'serpentine_variation_price_format', 10, 2 );

function serpentine_variation_price_format( $price, $product ) {

// Main Price
$prices = array( $product->get_variation_price( 'min', true ), $product->get_variation_price( 'max', true ) );
$price = $prices[0] !== $prices[1] ? sprintf( __( '%1$s', 'woocommerce' ), wc_price( $prices[0] ) ) : wc_price( $prices[0] );

// Sale Price
$prices = array( $product->get_variation_regular_price( 'min', true ), $product->get_variation_regular_price( 'max', true ) );
sort( $prices );
$saleprice = $prices[0] !== $prices[1] ? sprintf( __( '%1$s', 'woocommerce' ), wc_price( $prices[0] ) ) : wc_price( $prices[0] );

if ( $price !== $saleprice ) {
$price = '<del>' . $saleprice . '</del> <ins>' . $price . '</ins>';
}
return $price;
}


?>
