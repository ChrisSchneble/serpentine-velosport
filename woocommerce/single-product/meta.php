<?php
/**
 * Single Product Meta
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/meta.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see        https://docs.woocommerce.com/document/template-structure/
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     3.0.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

global $post, $product;

/*$cat_count = sizeof(get_the_terms($post->ID, 'product_cat'));
$tag_count = sizeof(get_the_terms($post->ID, 'product_tag'));*/

?>
<div class="product_meta">
    <?php do_action('woocommerce_product_meta_start'); ?>

    <div class="dropdown ss-no-variant-container">
        <button class="ss-btn btn-blue dropdown-toggle" type="button" data-toggle="dropdown">Gesuchte Variante nicht verfügbar ?
            <span class="caret"></span></button>
        <ul class="dropdown-menu">
            <li>
                <h4>Bitte nehmen sie Kontakt mit uns auf</h4>
                <span class="content">Du suchst oder wünschst weitere Informationen zu Verfügbarkeit,
                Farboptionen, Rahmengrößen oder Custom Anpassungen, dann schreib uns über
                unser Kontaktformular und du bekommst innerhalb Kürze eine Email von uns -
                oder du rufst uns an unter der 07731/799717. Danke!</span>
                <a class="link" href="<?php echo get_bloginfo('url') ?>/kontakt">Kontaktformular</a>
            </li>
        </ul>
    </div>

    <?php if (wc_product_sku_enabled() && ($product->get_sku() || $product->is_type('variable'))) : ?>
        <span class="sku_wrapper"><?php _e('SKU:', 'woocommerce'); ?> <span class="sku"
                                                                            itemprop="sku"><?php echo ($sku = $product->get_sku()) ? $sku : __('N/A', 'woocommerce'); ?></span></span>
    <?php endif; ?>

    <?php echo wc_get_product_category_list($product->get_id(), ', ', '<span class="posted_in">' . _n('Category:', 'Categories:', count($product->get_category_ids()), 'woocommerce') . ' ', '</span>'); ?>

    <?php echo wc_get_product_tag_list($product->get_id(), ', ', '<span class="tagged_as">' . _n('Tag:', 'Tags:', count($product->get_tag_ids()), 'woocommerce') . ' ', '</span>'); ?>

    <div class="ss-sharing-container top-30"><span>Artikel teilen:</span>
        <div class="social-link">
            <ul class="social-links clearfix">
                <li><a href="http://www.facebook.com/share.php?u=<?php the_permalink(); ?>" title="Facebook" target="_blank"
                       class="fa fa-facebook" rel="nofollow"></a></li>
                <li><a href="http://twitter.com/home?status=<?php the_permalink(); ?>" title="Twitter" target="_blank" class="fa fa-twitter"
                       rel="nofollow"></a></li>
                <li><a href="https://plus.google.com/share?url=<?php the_permalink(); ?>" title="Google" target="_blank"
                       class="fa fa-google-plus" rel="nofollow"></a></li>
                <li><a href="mailto:?<?php the_permalink(); ?>" title="Email" target="_blank" class="fa fa-envelope mail-link"
                       rel="nofollow"></a></li>
            </ul>
        </div>
    </div>

    <?php do_action('woocommerce_product_meta_end'); ?>

</div>
