<?php
/**
 * Serpentine functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Serpentine 1.0
 */


if (!function_exists('serpentine_setup')) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     *
     * Create your own serpentine_setup() function to override in a child theme.
     *
     * @since Serpentine 1.0
     */
    function serpentine_setup()
    {

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        /*
         * Enable support for custom logo.
         *
         *  @since Serpentine 1.2
         */
        add_theme_support('custom-logo', array(
            'height' => 240,
            'width' => 240,
            'flex-height' => true,
        ));

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
         */
        add_theme_support('post-thumbnails');
        set_post_thumbnail_size(1200, 9999);

        // This theme uses wp_nav_menu() in two locations.
        register_nav_menus(array(
            'primary' => __('Primary Menu', 'serpentine'),
            //'social' => __('Social Links Menu', 'serpentine'),
        ));

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ));

        /*
         * Enable support for Post Formats.
         *
         * See: https://codex.wordpress.org/Post_Formats
         */
        add_theme_support('post-formats', array(
            'aside',
            'image',
            'video',
            'quote',
            'link',
            'gallery',
            'status',
            'audio',
            'chat',
        ));

        /*
         * This theme styles the visual editor to resemble the theme style,
         * specifically font, colors, icons, and column width.
         */
        add_editor_style(array('css/editor-style.css', serpentine_fonts_url()));

        // Indicate widget sidebars can use selective refresh in the Customizer.
        add_theme_support('customize-selective-refresh-widgets');
    }
endif; // serpentine_setup
add_action('after_setup_theme', 'serpentine_setup');

/**
 * Registers a widget area.
 *
 */
function serpentine_widgets_init()
{
    register_sidebar(array(
        'name' => __('Sidebar', 'serpentine'),
        'id' => 'sidebar-1',
        'description' => __('Add widgets here to appear in your sidebar.', 'serpentine'),
        'before_widget' => '<div class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));

    register_sidebar(array(
        'name' => __('Footer Bereich', 'serpentine'),
        'id' => 'sidebar-2',
        'description' => __('Erscheint unterhalb des Main Contents jeder Site', 'serpentine'),
        'before_widget' => '<aside id="%1$s" class="widget wcol-4 col-xs-6 col-md-3 widget_text">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));

    register_sidebar(array(
        'name' => esc_html__('Shop Sidebar', 'serpentine'),
        'id' => 'woo-sidebar',
        'description' => esc_html__('Shop Sidebar.', 'serpentine'),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => "</aside>",
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>'
    ));
}

add_action('widgets_init', 'serpentine_widgets_init');


function serpentine_post_carousel_template_file( $filename, $instance, $widget ){
    if( !empty($instance['design']['theme']) && $instance['design']['theme'] == 'test' ) {
        $filename = get_stylesheet_directory() . '/tpl/base.php';
    }
    return $filename;
}
add_filter( 'siteorigin_widgets_template_file_sow-carousel', 'serpentine_post_carousel_template_file', 10, 3 );
add_filter( 'siteorigin_widgets_template_file_sow-post-carousel', 'serpentine_post_carousel_template_file', 10, 3 );

//apply_filters('siteorigin_widgets_template_file_' . $this->id_base, $template_file, $instance, $this );


if (!function_exists('serpentine_fonts_url')) :
    /**
     * Register Google fonts for Serpentine.
     *
     * Create your own serpentine_fonts_url() function to override in a child theme.
     *
     * @since Serpentine 1.0
     *
     * @return string Google fonts URL for the theme.
     */
    function serpentine_fonts_url()
    {
        $fonts_url = '';
        $fonts = array();
        $subsets = 'latin,latin-ext';

        /* translators: If there are characters in your language that are not supported by Merriweather, translate this to 'off'. Do not translate into your own language. */
        if ('off' !== _x('on', 'Merriweather font: on or off', 'serpentine')) {
            $fonts[] = 'Merriweather:400,700,900,400italic,700italic,900italic';
        }

        /* translators: If there are characters in your language that are not supported by Montserrat, translate this to 'off'. Do not translate into your own language. */
        if ('off' !== _x('on', 'Montserrat font: on or off', 'serpentine')) {
            $fonts[] = 'Montserrat:400,700';
        }

        /* translators: If there are characters in your language that are not supported by Inconsolata, translate this to 'off'. Do not translate into your own language. */
        if ('off' !== _x('on', 'Inconsolata font: on or off', 'serpentine')) {
            $fonts[] = 'Inconsolata:400';
        }

        if ($fonts) {
            $fonts_url = add_query_arg(array(
                'family' => urlencode(implode('|', $fonts)),
                'subset' => urlencode($subsets),
            ), 'https://fonts.googleapis.com/css');
        }

        return $fonts_url;
    }
endif;


/**
 * Include required files
 */

require_once(trailingslashit(get_template_directory()) . 'woocommerce/woocommerce-hooks.php');


function excerpt($limit)
{
    $excerpt = explode(' ', get_the_excerpt(), $limit);
    if (count($excerpt) >= $limit) {
        array_pop($excerpt);
        $excerpt = implode(" ", $excerpt) . '...';
    } else {
        $excerpt = implode(" ", $excerpt);
    }
    $excerpt = preg_replace('`\[[^\]]*\]`', '', $excerpt);
    return $excerpt;
}

function content($limit)
{
    $content = explode(' ', get_the_content(), $limit);
    if (count($content) >= $limit) {
        array_pop($content);
        $content = implode(" ", $content) . '...';
    } else {
        $content = implode(" ", $content);
    }
    $content = preg_replace('/\[.+\]/', '', $content);
    $content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]&gt;', $content);
    return $content;
}


function the_title_limit($length, $replacer = '...')
{
    $string = the_title('', '', FALSE);
    if (strlen($string) > $length)
        $string = (preg_match('/^(.*)\W.*$/', substr($string, 0, $length + 1), $matches) ? $matches[1] : substr($string, 0, $length)) . $replacer;
    echo $string;
}

function productOnSaleFlag($product) {
    $saleInfo = null;
    if ($product->is_on_sale()) {
        $saleInfo = "<span class=\"sale\">sale</span>";
    }
    return $saleInfo;
}

function productNewFlag($product) {
    $attributes = $product->get_attributes();
    $attribute = isset($attributes['neu']) ? $attributes['neu'] : '';
    $newInfo = null;
    if ($attribute ) {
        $newInfo = "<span class=\"sale\">neu!</span>";
    }
    return $newInfo;
}

function getFirstCatId($post) {
    $terms =  get_the_terms( $post->ID, 'product_cat' );
    if ( $terms && ! is_wp_error( $terms ) ) {
        echo $terms[0]->name;
    }
}

function printActualPrice($product) {
    $prices = array( $product->get_variation_price( 'min', true ), $product->get_variation_price( 'max', true ) );
    $price = $prices[0] !== $prices[1] ? sprintf( __( '%1$s', 'woocommerce' ), wc_price( $prices[0] ) ) : wc_price( $prices[0] );

    // Sale Price
    $prices = array( $product->get_variation_regular_price( 'min', true ), $product->get_variation_regular_price( 'max', true ) );
    sort( $prices );
    $salePrice = $prices[0] !== $prices[1] ? sprintf( __( '%1$s', 'woocommerce' ), wc_price( $prices[0] ) ) : wc_price( $prices[0] );

    $printPrice = null;
    $currentPrice = null;
    if (!empty($price)) {
        if (!empty($sale)) {
            $currentPrice = $sale;
        } else {
            $currentPrice = $price;
        }
        $printPrice = $currentPrice .= " €";
        return $printPrice;
    }
}


/*********************************************************************/
/*********************************************************************/
add_filter('woocommerce_variable_sale_price_html', 'my_variation_price_format', 10, 2);
add_filter('woocommerce_variable_price_html', 'my_variation_price_format', 10, 2);
function my_variation_price_format($price, $product) {
    // Main Price
    $prices = array($product->get_variation_price('min', true), $product->get_variation_price('max', true));
    $price = $prices[0] !== $prices[1] ? sprintf(__('%1$s', 'woocommerce'), wc_price($prices[0])) : wc_price($prices[0]);
    // Sale Price
    $prices = array($product->get_variation_regular_price('min', true), $product->get_variation_regular_price('max', true));
    sort($prices);
    $saleprice = $prices[0] !== $prices[1] ? sprintf(__('%1$s', 'woocommerce'), wc_price($prices[0])) : wc_price($prices[0]);

    if ($price !== $saleprice) {
        $price = '<span class="uvpFlag">Statt UVP***</span><del>' . $saleprice . '</del> <ins>' . $price . '</ins>';
    }
    return $price;
}

add_filter('woocommerce_available_variation', function ($value, $object = null, $variation = null) {
    $saleInfo = null;
    if ($variation->is_on_sale()) {
        $saleInfo = "<span class=\"uvpFlag\">Statt UVP***</span>";
    }
    $value['price_html'] = $saleInfo . $variation->get_price_html() . '</span>';
    return $value;
}, 10, 3);


/**
 * Handles JavaScript detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since Serpentine 1.0
 */
function serpentine_javascript_detection()
{
    echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}

add_action('wp_head', 'serpentine_javascript_detection', 0);

/**
 * Enqueues scripts and styles.
 *
 * @since Serpentine 1.0
 */
function serpentine_scripts()
{
    // Add custom fonts, used in the main stylesheet.
    wp_enqueue_style('serpentine-fonts', serpentine_fonts_url(), array(), null);


    // Theme stylesheet.
    wp_enqueue_style('serpentine-style', get_stylesheet_uri());
    wp_enqueue_style('fonts', get_template_directory_uri() . '/css/serpentine.icons.css', array(), '29102016');
    wp_enqueue_style('responsive', get_template_directory_uri() . '/css/responsive.css', array(), '29102016');
    wp_enqueue_style('woocommerce-custom', get_template_directory_uri() . '/woocommerce/woocommerce-custom.css', array(), 'woocommerce');


    //Theme Scripts
    wp_enqueue_script('smoothscroll', get_template_directory_uri() . '/js/smoothScroll.js', array('jquery'), '29102016', true);
    wp_enqueue_script('izotope', get_template_directory_uri() . '/js/isotope.pkgd.min.js', array('jquery'), '29102016', true);
    wp_enqueue_script('dropdown', get_template_directory_uri() . '/js/bootstrap-dropdown.js', array('jquery'), '29102016', true);
/*    wp_enqueue_script('viewportchecker', get_template_directory_uri() . '/js/owl.carousel.min.js', array('jquery'), '29102016', true);*/
    wp_enqueue_script('serpentine-script', get_template_directory_uri() . '/js/app.js', array('jquery'), '29102016', true);
}

add_action('wp_enqueue_scripts', 'serpentine_scripts');

/**
 * Adds custom classes to the array of body classes.
 *
 * @since Serpentine 1.0
 *
 * @param array $classes Classes for the body element.
 * @return array (Maybe) filtered body classes.
 */
function serpentine_body_classes($classes)
{
    // Adds a class of custom-background-image to sites with a custom background image.
    if (get_background_image()) {
        $classes[] = 'custom-background-image';
    }

    // Adds a class of group-blog to sites with more than 1 published author.
    if (is_multi_author()) {
        $classes[] = 'group-blog';
    }

    // Adds a class of no-sidebar to sites without active sidebar.
    if (!is_active_sidebar('sidebar-1')) {
        $classes[] = 'no-sidebar ';
    }

    // Adds a class of hfeed to non-singular pages.
    if (!is_singular()) {
        $classes[] = 'hfeed';
    }

    if (is_front_page()) {
        $classes[] = 'transparent-nav';
    }

    if (is_post_type_archive('product') || is_tax(get_object_taxonomies('product'))) {
        $classes[] = 'sidebar-left';
    }


    $classes[] = 'is-stretched fixed-nav';

    return $classes;
}

add_filter('body_class', 'serpentine_body_classes');

/**
 * Converts a HEX value to RGB.
 *
 * @since Serpentine 1.0
 *
 * @param string $color The original color, in 3- or 6-digit hexadecimal form.
 * @return array Array containing RGB (red, green, and blue) values for the given
 *               HEX code, empty array otherwise.
 */
function serpentine_hex2rgb($color)
{
    $color = trim($color, '#');

    if (strlen($color) === 3) {
        $r = hexdec(substr($color, 0, 1) . substr($color, 0, 1));
        $g = hexdec(substr($color, 1, 1) . substr($color, 1, 1));
        $b = hexdec(substr($color, 2, 1) . substr($color, 2, 1));
    } else if (strlen($color) === 6) {
        $r = hexdec(substr($color, 0, 2));
        $g = hexdec(substr($color, 2, 2));
        $b = hexdec(substr($color, 4, 2));
    } else {
        return array();
    }

    return array('red' => $r, 'green' => $g, 'blue' => $b);
}
add_theme_support( 'post-thumbnails' );


function wpdocs_theme_setup()
{
    add_image_size('shopItem-thumb', 526, 350, true);
    add_image_size('cube-thumb', 576, 576, true );
}

add_action('after_setup_theme', 'wpdocs_theme_setup');

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for content images
 *
 * @since Serpentine 1.0
 *
 * @param string $sizes A source size value for use in a 'sizes' attribute.
 * @param array $size Image size. Accepts an array of width and height
 *                      values in pixels (in that order).
 * @return string A source size value for use in a content image 'sizes' attribute.
 */
//function serpentine_content_image_sizes_attr($sizes, $size)
//{
//    $width = $size[0];
//
//    840 <= $width && $sizes = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 62vw, 840px';
//
//    if ('page' === get_post_type()) {
//        840 > $width && $sizes = '(max-width: ' . $width . 'px) 85vw, ' . $width . 'px';
//    } else {
//        840 > $width && 600 <= $width && $sizes = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 61vw, (max-width: 1362px) 45vw, 600px';
//        600 > $width && $sizes = '(max-width: ' . $width . 'px) 85vw, ' . $width . 'px';
//    }
//
//    return $sizes;
//}

//add_filter('wp_calculate_image_sizes', 'serpentine_content_image_sizes_attr', 10, 2);


/**
 * Modifies tag cloud widget arguments to have all tags in the widget same font size.
 *
 * @since Serpentine 1.1
 *
 * @param array $args Arguments for tag cloud widget.
 * @return array A new modified arguments.
 */
function serpentine_widget_tag_cloud_args($args)
{
    $args['largest'] = 1;
    $args['smallest'] = 1;
    $args['unit'] = 'em';
    return $args;
}

add_filter('widget_tag_cloud_args', 'serpentine_widget_tag_cloud_args');


/**
 * Disable responsive image support
 */

// Clean the up the image from wp_get_attachment_image()
add_filter( 'wp_get_attachment_image_attributes', function( $attr )
{
    if( isset( $attr['sizes'] ) )
        unset( $attr['sizes'] );

    if( isset( $attr['srcset'] ) )
        unset( $attr['srcset'] );

    return $attr;

 }, PHP_INT_MAX );

// Override the calculated image sizes
add_filter( 'wp_calculate_image_sizes', '__return_false',  PHP_INT_MAX );

// Override the calculated image sources
add_filter( 'wp_calculate_image_srcset', '__return_false', PHP_INT_MAX );

// Remove the reponsive stuff from the content
remove_filter( 'the_content', 'wp_make_content_images_responsive' );

function remove_img_attr ($html) {
    return preg_replace('/(width|height)="\d+"\s/', "", $html);
}

add_filter( 'post_thumbnail_html', 'remove_img_attr' );


//removes jquery migrate
add_action( 'wp_default_scripts', function( $scripts ) {
    if ( ! empty( $scripts->registered['jquery'] ) ) {
        $jquery_dependencies = $scripts->registered['jquery']->deps;
        $scripts->registered['jquery']->deps = array_diff( $jquery_dependencies, array( 'jquery-migrate' ) );
    }
} );

// Display 12 products per page.
add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 12;' ), 20 );


// remove emoji
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

function serpentine_widgets_collection($folders)
{
    $folders[] = get_template_directory() . '/widgets/';
    return $folders;
}

add_filter('siteorigin_widgets_widget_folders', 'serpentine_widgets_collection');

add_action( 'after_setup_theme', 'setup_woocommerce_support' );

 function setup_woocommerce_support()
{
  add_theme_support('woocommerce');
}

add_action( 'after_setup_theme', 'yourtheme_setup' );

function yourtheme_setup() {
add_theme_support( 'wc-product-gallery-zoom' );
add_theme_support( 'wc-product-gallery-lightbox' );
add_theme_support( 'wc-product-gallery-slider' );
}


