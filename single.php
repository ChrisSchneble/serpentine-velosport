<?php
/**
 * The template for displaying all single posts
 *
 */

get_header(); ?>

	<main id="main" class="col-md-12" role="main">
		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

			// Include the single post content template.
			get_template_part( 'template-parts/content', 'single' );

			/*// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) {
				comments_template();
			}*/

			if ( is_singular( 'post' ) ) {
				// Previous/next post navigation.
				the_post_navigation( array(
					'next_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Next', 'serpentine' ) . '</span> ' .
						'<span class="screen-reader-text">' . __( 'Next post:', 'serpentine' ) . '</span> ' .
						'<span class="post-title">%title</span>',
					'prev_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Previous', 'serpentine' ) . '</span> ' .
						'<span class="screen-reader-text">' . __( 'Previous post:', 'serpentine' ) . '</span> ' .
						'<span class="post-title">%title</span>',
				) );
			}

			// End of the loop.
		endwhile;
		?>

	</main><!-- .site-main -->
<?php get_footer(); ?>
