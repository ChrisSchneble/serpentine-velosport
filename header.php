<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Serpentine 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="initial-scale=1, width=device-width">
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri() . '/images/favicon.ico'; ?>">

    <?php if (is_singular() && pings_open(get_queried_object())) : ?>
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <?php endif; ?>
    <?php wp_head(); ?>
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800,300italic,400italic,600italic,700italic,800italic">
          <link href="https://fonts.googleapis.com/css?family=Armata" rel="stylesheet">
</head>

<body <?php body_class(); ?>>

<!-- Mobile Menu -->
<div id="resp-menu">

    <div class="resp-menu-logo">
        <a href="<?php echo get_bloginfo('url') ?>" class="custom-logo-link" rel="home" itemprop="url">
            <img width="240" height="48" src="<?php echo get_template_directory_uri() . '/images/serpentine_logo.svg'; ?>" class="custom-logo" alt="" itemprop="logo">
        </a>
    </div><!-- /.resp-menu-logo -->

    <?php
    $menu_args = array('theme_location' => 'primary', 'menu_class' => 'resp-menu', 'container' => false);
    $hide_top_nav = '';
    wp_nav_menu($menu_args);
    ?>
</div><!--/ #resp-menu -->
<span class="mobile-close menu-trigger">
    <i class=" mdi mdi-close"></i>
</span>



<div id="page" class="hfeed site clearfix ">
    <a class="skip-link screen-reader-text" href="#content"><?php wp_enqueue_script('Skip to content', 'serpentine'); ?></a>

    <header id="header" class="site-header clearfix normal">
        <div class="header-main">
            <div class="container clearfix">
                <div class="row">

                    <div class="header-menu-icon">
                        <a href="#" class="menu-trigger" title="Toggle Menu"><i class="mdi mdi-menu"></i><span
                                class="sr-only">menu</span></a>
                    </div><!-- /.header-menu-icon -->

                    <div class="brand">
                        <h1 class="site-title logo-image">
                            <a href="<?php echo get_bloginfo('url') ?>" class="custom-logo-link" rel="home" itemprop="url">
                                <img width="240" height="48" src="<?php echo get_template_directory_uri() . '/images/serpentine_logo.svg'; ?>" class="custom-logo" alt="" itemprop="logo">
                            </a>
                        </h1>
                    </div><!-- /.col-sm-3 -->
                    <?php if (has_nav_menu('primary')) : ?>

                        <div class="menu-area clearfix">
                            <nav id="main-nav" class="primary-nav">
                                <?php $menu_args = array('theme_location' => 'primary', 'menu_class' => 'nav-menu clearfix', 'container' => '', 'container_class' => false);
                                wp_nav_menu($menu_args);
                                ?>
                            </nav><!-- /#main-nav -->

                            <div class="header-icons">
                                <div class="header-search-widget">
                                    <a href="#" class="search-trigger" title="Search"><i class="mdi mdi-search"></i><span
                                            class="sr-only">search</span></a>
                                </div><!-- /.header-search-widget -->
<!--                                <div class="header-wishlist-count">-->
<!--                                    <a href="#" class="wishlist-button"-->
<!--                                       title="View your wishlist"><i class="mdi mdi-favorite"></i><span-->
<!--                                            class="sr-only">wishlist</span><span-->
<!--                                            class="wishlist-count">0</span></a>-->
<!--                                </div>-->
                                <div class="cart-widget woocommerce widget_shopping_cart">
                                    <a class="cart-contents-2" href="#" title="View your shopping cart"><i
                                            class="mdi mdi-shopping_cart"></i><span class="cart-count">0</span></a>

                                    <div class="cart-submenu">
                                        <div class="widget_shopping_cart_content">
                                        </div><!-- /.widget_shopping_cart_content -->
                                    </div><!-- /.cart-submenu -->
                                </div><!-- /.cart-widget -->
                            </div><!-- /.header-menu-icons -->
                        </div><!-- /.menu-area -->
                    <?php endif; ?>
                </div>
            </div>

            <div class="search-panel">
                <div class="container clearfix">
                    <form method="get" class="woocommerce-product-search" action="<?php $search_query = get_search_query(); ?>">
                        <label class="screen-reader-text">Search for:</label>
                        <input type="search" class="search-field" placeholder="Suchbegriff eingeben…" value="" name="s" title="Suche nach:">
                        <input type="submit" value="Search">
                        <!--<input type="hidden" name="post_type" value="product">-->
                    </form>
                    <a href="#" class="search-close" title="Close"><i class="mdi mdi-close"></i><span class="sr-only">suche schließen</span></a>
                </div><!-- /.container -->
            </div>
        </div>
    </header><!-- .site-header -->

    <div id="primary">
        <div id="content" class="site-content container clearfix">
            <div class="row content-row">
