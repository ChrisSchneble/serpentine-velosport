<?php
$query = array( 'post_type' => 'product', 'posts_per_page' => 10, 'product_cat' => 'cannondale', 'order' => 'DESC' );
$posts = new WP_Query($query);
?>

<?php if ($posts->have_posts()) : ?>
    <div class="sow-carousel-title">
<!--        --><?php //echo $args['before_title'] . esc_html($instance['title']) . $args['after_title'] ?>

        <a href="#" class="sow-carousel-next" title="<?php esc_attr_e('Next', 'so-widgets-bundle') ?>"></a>
        <a href="#" class="sow-carousel-previous" title="<?php esc_attr_e('Previous', 'so-widgets-bundle') ?>"></a>
    </div>

    <div class="sow-carousel-container<?php if (is_rtl()) echo ' js-rtl' ?>">

        <a href="#" class="sow-carousel-previous" title="<?php esc_attr_e('Previous', 'so-widgets-bundle') ?>"></a>

        <a href="#" class="sow-carousel-next" title="<?php esc_attr_e('Next', 'so-widgets-bundle') ?>"></a>

        <div class="sow-carousel-wrapper"
             data-query="<?php echo esc_attr($instance['posts']) ?>"
             data-found-posts="<?php echo esc_attr($posts->found_posts) ?>"
             data-ajax-url="<?php echo sow_esc_url(wp_nonce_url(admin_url('admin-ajax.php'), 'widgets_action', '_widgets_nonce')) ?>">
            <ul class="sow-carousel-items">
                <?php
                $posts = new WP_Query($query);
                while ($posts->have_posts()) : $posts->the_post(); ?>
                    <li class="sow-carousel-item<?php if (is_rtl()) echo ' rtl' ?>">
                        <div class="sow-carousel-thumbnail">
                            <?php if (has_post_thumbnail()) : $img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'sow-carousel-default'); ?>
                                <a href="<?php the_permalink() ?>" style="background-image: url(<?php echo sow_esc_url($img[0]) ?>)">
                                    <span class="overlay"></span>
                                </a>
                            <?php else : ?>
                                <a href="<?php the_permalink() ?>" class="sow-carousel-default-thumbnail"><span class="overlay"></span></a>
                            <?php endif; ?>
                        </div>
                        <?php global $product, $post; ?>
                        <div class="collection-category"><?php echo get_cat_name(getFirstCatId($post)); ?></div>
                        <div class="collection-description"><h2 title="<?php the_title() ?>"><?php the_title_limit(20, '...'); ?></h2>
                            <p class="collection-excerpt"><?php echo excerpt(10); ?></p>
                            <div class="collection-actions">
                                <a class="collection-action-btn" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">Jetzt
                                    Kaufen</a><?php echo productNewFlag($product) ?><?php echo productOnSaleFlag($product) ?><span
                                        class="price-range"><?php echo printActualPrice($product); ?></span></div>
                        </div>
                    </li>
                <?php endwhile;
                wp_reset_postdata(); ?>
            </ul>
        </div>
    </div>
<?php endif; ?>
