<div class="fluid-text bottom-24 x2 panel-widget-style">
    <div class="ss-tile has-overlay with-link">
        <div class="ss-overlay top-left"><?php the_title('<h2>', '</h2>'); ?>
            <p><?php echo excerpt(25); ?></p></div>
        <div class="ss-overlay bottom-left"><p><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"
                                                  class="ss-btn btn-flat">Mehr</a></p></div>
        <a class="img-link" href="<?php the_permalink(); ?>"><?php the_post_thumbnail('cube-thumb'); ?></a></div>
</div>

