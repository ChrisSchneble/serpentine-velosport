<?php
/**
 * The template part for displaying single posts
 *
 */
?>

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="entry-content">
        <section class="section parallax section-inverse"
                 data-bgimage="<?php echo the_post_thumbnail_url( 'full' ); ?>" data-speed="3">
            <div class="color-overlay dark">
                <div class="container">
                    <?php the_title('<h2 class="text-xl bottom-8">', '</h2>');?>
                    <h3><?php echo excerpt(20) ?></h3></div>
            </div>
        </section>
        <div class="entry-content">
            <?php the_content(); ?>
        </div>
        <div class="ss-sharing-container">
                <span>Beitrag teilen:</span>
                <div class="social-link">
                    <ul class="social-links clearfix">
                        <li><a href="http://www.facebook.com/share.php?u=<?php the_permalink(); ?>" title="Facebook" target="_blank" class="fa fa-facebook" rel="nofollow"></a></li>
                        <li><a href="http://twitter.com/home?status=<?php the_permalink(); ?>" title="Twitter" target="_blank" class="fa fa-twitter" rel="nofollow"></a></li>
                        <li><a href="https://plus.google.com/share?url=<?php the_permalink(); ?>" title="Google" target="_blank" class="fa fa-google-plus" rel="nofollow"></a></li>
                        <li><a href="mailto:?<?php the_permalink(); ?>" title="Email" target="_blank" class="fa fa-envelope mail-link" rel="nofollow"></a></li>
                    </ul>
                </div>
            </div>
    </div>
</div>

