<?php
/**
 * The template part for displaying a message that posts cannot be found
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Serpentine 1.0
 */
?>


<section class="no-results not-found center">
    <header class="page-header">
        <h1 class="page-title text-xl text-red"><?php _e('Nothing Found', 'serpentine'); ?></h1>
    </header><!-- .page-header -->

    <div class="page-content">
        <?php if (is_home() && current_user_can('publish_posts')) : ?>

            <p><?php printf(__('Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'serpentine'), esc_url(admin_url('post-new.php'))); ?></p>

        <?php elseif (is_search()) : ?>

            <p><?php _e('Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'serpentine'); ?></p>
            <?php get_search_form(); ?>

        <?php else : ?>

            <p><?php _e('It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'serpentine'); ?></p>
            <?php get_search_form(); ?>

        <?php endif; ?>
        <p>
            <a class="ss-btn btn-theme" href="<?php echo get_bloginfo('url') ?>/">Zur Startseite</a>
        </p>
    </div><!-- .page-content -->
</section><!-- .no-results -->
