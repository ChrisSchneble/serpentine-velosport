<?php
global $product, $post;
?>

<div class="animated bottom-24 panel-widget-style invisible visible fadeIn" data-animation="fadeIn" data-animdelay="100">
    <div class="ss-product-collection">
        <div class="image-set"><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('shopItem-thumb'); ?></a></div>
        <div class="collection-category"><?php echo get_cat_name(getFirstCatId($post)); ?></div>
        <div class="collection-description"><h2 title="<?php the_title() ?>"><?php the_title_limit(20, '...'); ?></h2>
            <p class="collection-excerpt"><?php echo excerpt(10); ?></p>
            <div class="collection-actions">
                <a class="collection-action-btn" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">Jetzt
                    Kaufen</a><?php echo productNewFlag($product) ?><?php echo productOnSaleFlag($product) ?><span class="price-range"><?php echo printActualPrice($product); ?></span></div>
        </div>
    </div>
</div>
