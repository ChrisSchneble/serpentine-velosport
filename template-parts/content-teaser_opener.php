<?php if (class_exists('Dynamic_Featured_Image')) {
    global $dynamic_featured_image;
    global $post;
    $featured_images = $dynamic_featured_image->get_featured_images($post->ID);
    $bg_images = [];
    $i = 0;
    if ($featured_images):
        ?>
        <?php foreach ($featured_images as $images): ?>
        <?php
        ?>
    <?php endforeach; ?>
        <?php
    endif;
}
?>

<section class="section parallax section-inverse opener">
    	<ul class="slider" data-animation="<?php echo (sizeof($featured_images) + 1)*9?>">
    		<li style="background-image: url(<?php echo the_post_thumbnail_url( 'full' ); ?>);"></li>
            <?php foreach ($featured_images as $images): ?>
                <?php $i++; ?>
                <li style="background-image: url(<?php echo $images['full']; ?>); -webkit-animation-delay: <?php echo $i*9?>s; -moz-animation-delay: <?php echo $i*9 ?>s; animation-delay: <?php echo $i*9 ?>s;"></li>
            <?php endforeach; ?>
    	</ul>
    <div class="force-full-screen">
        <div class="container">
            <div class="absolute-center">
                <h2 class="text-xl mobile-heading"><?php the_title_limit( 25, ''); ?></h2>
                <p class="h3"><?php echo excerpt(25); ?></p>
                <p>
                    <a class="ss-btn btn-blue" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">Erfahre mehr</a>
                </p>
                <p class="goto-next">
                    <a class="arrow ci bounce" href="#scroller" ><i class="fa fa-angle-down"></i></a>
                </p>
            </div>
        </div>
    </div>
</section>
