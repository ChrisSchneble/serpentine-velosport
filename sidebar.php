<?php
/**
 * The sidebar containing the main widget area.
 */

global $post;
?>

<div id="sidebar" class="widget-area col-xs-12 col-md-3" role="complementary">
	<?php

	// Load shop sidebar on WooCommerce Pages
    if ( ( is_post_type_archive( 'product' ) || is_tax( get_object_taxonomies( 'product' ) ) ) && is_active_sidebar( 'woo-sidebar' ) ) { ?>

        <div id="woo-sidebar">
            <div class="wrap">
				<?php dynamic_sidebar( 'woo-sidebar' ); ?>
            </div><!-- /.wrap -->
        </div><!-- /#woo-sidebar -->

    <?php }

    else {

		if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
            <div class="main-sidebar">
				<?php dynamic_sidebar( 'sidebar-1' ); ?>
            </div><!-- /.main-sidebar -->
		<?php
		endif;
    }
	?>
</div><!-- #sidebar -->
