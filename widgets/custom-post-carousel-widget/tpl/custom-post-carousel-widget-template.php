
<?php
$post_selector_pseudo_query = $instance['some_posts'];
// Process the post selector pseudo query.
$processed_query = siteorigin_widget_post_selector_process_query($post_selector_pseudo_query);

// Use the processed post selector query to find posts.
$query_result = new WP_Query($processed_query);

if ($query_result->have_posts()) : ?>
    <div class="product-carousel" data-items="4" data-loop="false" data-speed="300" data-margin="24" data-margin_mobile="16" data-autoplay="false" data-timeout="5000"
         data-autoheight="false" data-nav="true" data-dots="false">
        <div class="products column-3">
            <?php while ($query_result->have_posts()) : $query_result->the_post(); ?>
                <?php get_template_part( wp_kses_post($instance['template']) ); ?>
            <?php endwhile;
            wp_reset_postdata(); ?>

        </div>
    </div>
<?php endif; ?>

