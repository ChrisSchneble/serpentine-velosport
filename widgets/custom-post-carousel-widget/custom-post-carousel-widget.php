<?php

/*
Widget Name: Custom Post Carousel Widget
Description: Widget To Custom Posts in Slider
Author: Christian Schneble
Author URI: http://destinationstudios.christianschneble.de
*/

class Custom_Post_Carousel_Widget extends SiteOrigin_Widget {
	function __construct() {

		parent::__construct(
			'custom-post-carousel-widget',
			__('Custom Post Carousel Widget', 'custom-post-carousel-widget-text-domain'),
			array(
				'description' => __('Custom_Post_Carousel widget.', 'custom-post-carousel-widget-text-domain'),
			),
			array(

			),
			array(
                'template' => array(
                    'type' => 'text',
                    'label' => __('template', 'widget-form-fields-text-domain'),
                    'default' => 'template-parts/content-teaser_shopItem'
                ),
                'some_posts' => array(
                    'type' => 'posts',
                    'label' => __('Some posts query', 'widget-form-fields-text-domain'),
                )
			),
			plugin_dir_path(__FILE__)
		);
	}

	function get_template_name($instance) {
		return 'custom-post-carousel-widget-template';
	}

	function get_style_name($instance) {
		return 'custom-post-carousel-widget-style';
	}

}

siteorigin_widget_register('custom-post-carousel-widget', __FILE__, 'Custom_Post_Carousel_Widget');
