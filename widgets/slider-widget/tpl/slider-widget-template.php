<?php
global $post;
$searchargs = array( 'posts_per_page' => 5, 'offset'=> 0, 'category' => wp_kses_post($instance['categoryId']) );
$myposts = get_posts( $searchargs );
?>

<div class="homepage-slider" data-timeout="<?php echo wp_kses_post($instance['slideTimeout']) ?>">
    <div data-am-gallery>
        <!-- Radio -->
        <?php $radio = 0; ?>
        <?php foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
            <?php $isChecked = $radio+1 == 1 ? 'checked' : '' ?>
            <input type="radio" name="gallery" id="img-<?php echo $radio+1 ?>" <?php echo $isChecked ?> />
            <?php $radio++; ?>
        <?php endforeach; wp_reset_postdata();?>
        <!-- Images -->
        <div class="images">
            <?php foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
                <section class="parallax section-inverse image" style="background-image: url(<?php echo the_post_thumbnail_url( 'full' ); ?>);">
                    <div class="color-overlay dark force-full-screen" style="background: rgba(0, 0, 0, <?php echo wp_kses_post($instance['overlayOpacity'])/100 ?>);">
                        <div class="container">
                            <div class="absolute-center">
                                <h2 class="text-xl mobile-heading"><?php the_title_limit( 25, ''); ?></h2>
                                <p class="h3"><?php echo excerpt(25); ?></p>
                                <p>
                                    <a class="ss-btn <?php echo wp_kses_post($instance['button_color']) ?>" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">Erfahre mehr</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </section>
            <?php endforeach; wp_reset_postdata();?>
        </div>
        <!-- Navigation -->
        <div class="navigation">
            <?php $dots = 0; ?>
            <?php foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
                <label class="dot" for="img-<?php echo $dots+1 ?>"></label>
                <?php $dots++; ?>
            <?php endforeach; wp_reset_postdata();?>
        </div>
    </div>
</div>
