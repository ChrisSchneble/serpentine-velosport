<?php

/*
Widget Name: Slider Widget
Description: Widget To Show Slider'.
Author: Christian Schneble
Author URI: http://destinationstudios.christianschneble.de
*/

class Slider_Widget extends SiteOrigin_Widget {
	function __construct() {

		parent::__construct(
			'slider-widget',
			__('Slider Widget', 'slider-widget-text-domain'),
			array(
				'description' => __('Slider widget.', 'slider-widget-text-domain'),
			),
			array(

			),
			array(
                'slideTimeout' => array(
                    'type' => 'number',
                    'label' => __('Pause in Millisekunden', 'slider-widget-text-domain'),
                    'default' => '5000'
                ),
                'categoryId' => array(
                    'type' => 'number',
                    'label' => __('CategoryId', 'slider-widget-text-domain'),
                    'default' => '66'
                ),
                'button_color' => array(
                    'type' => 'text',
                    'label' => __('Link/Button Farbe', 'widget-form-fields-text-domain'),
                    'default' => 'btn-blue'
                ),
                'overlayOpacity' => array(
                    'type' => 'slider',
                    'label' => __('Hintergrundüberlagerung in Prozent', 'widget-form-fields-text-domain'),
                    'default' => 52,
                    'min' => 1,
                    'max' => 100,
                    'integer' => true
                )
			),
			plugin_dir_path(__FILE__)
		);
	}

	function get_template_name($instance) {
		return 'slider-widget-template';
	}

	function get_style_name($instance) {
		return 'slider-widget-style';
	}

}

siteorigin_widget_register('slider-widget', __FILE__, 'Slider_Widget');
