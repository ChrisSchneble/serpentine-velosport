<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Serpentine 1.0
 */
?>
</div><!-- /.content-row -->
</div><!-- /.site-content -->
</div><!-- /#primary -->


<div id="secondary-b" class="secondary widget-area" role="complementary">
    <div class="container clearfix">
        <div class="row gutter-24 sec-row">
            <?php get_sidebar('content-bottom'); ?>
        </div><!-- /.sec-row -->
    </div><!-- /.container -->
</div><!-- /#secondary-b -->

<footer id="footer">
    <div class="container clearfix">
        <div class="row text-center">
            <div class="ss-col col-md-12 text-12">
                <a href="<?php echo get_bloginfo('url') ?>/kontakt#impressum">Impressum</a> und <a href="<?php echo get_bloginfo('url') ?>/disclaimer">Disclaimer</a> | © <?php echo date("Y"); ?> Serpentine</a>
            </div>
        </div>
    </div><!-- /.container -->
</footer><!-- .site-footer -->
<div id="scroll-to-top" class="scroll-to-top">
    <a href="#" title="Scroll to top"><span class="sr-only"></span></a>
</div><!-- /.scroll-to-top -->

<div class="resp-menu-mask"></div><!-- /.resp-menu-mask -->
</div><!-- .site-inner -->
</div><!-- .site -->


<?php wp_footer(); ?>
<div id="blockingOverlay" class="blockingOverlay animated"></div>
</body>
</html>
