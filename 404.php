<?php
/**
 * The template for displaying 404 pages (Not Found).
 */

get_header(); ?>

<main id="main" class="col-xs-12 top-100 bottom-80">
    <div class="error-404 not-found center">
        <header class="page-header">
            <h1 class="page-title text-xl text-red"><?php esc_html_e('Du hast einen Platten.', 'serpentine'); ?></h1>
        </header><!-- /.page-header -->

        <div class="page-content">
            <p class="text-m"><?php esc_html_e('Leider gibt es hier nicht, was du gesucht hast. Vielleicht hilft dir die Suchfunktion!?', 'serpentine'); ?></p>
            <?php get_search_form(); ?>
        </div><!-- /.page-content -->
        <p>
            <a class="ss-btn btn-theme" href="<?php echo get_bloginfo('url') ?>/">Zur Startseite</a>
        </p>
    </div><!-- /.error-404 -->
</main><!-- /#main -->

<?php get_footer(); ?>
